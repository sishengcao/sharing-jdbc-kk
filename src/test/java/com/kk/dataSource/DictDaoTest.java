package com.kk.dataSource;

import com.kk.service.IAgentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


/**
 * @auther sishengcao
 * @date 2022/4/13 15:33
 */
@SpringBootTest(classes = DictDaoTest.class)
@Slf4j
public class DictDaoTest {

    @Autowired
    private IAgentService agentService;
}
