package com.kk.common.response;

import com.kk.common.menu.ResultEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @auther sishengcao
 * @date 2022/4/13 17:14
 */
@Setter
@Getter
public class Result implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer code;
    private String message;
    private Object data;

    public static Result getResult() {
        return new Result();
    }

    public static Result success() {
        Result result = getResult();
        result.setCode(ResultEnum.SUCCESS.getCode());
        return result;
    }

    public static Result success(Integer errorCode, String message, Object data) {
        Result result = getResult();
        result.setCode(errorCode);
        result.setMessage(message);
        result.setData(data);
        return result;
    }

    public static Result success(Integer errorCode, String message) {
        Result result = getResult();
        result.setCode(errorCode);
        result.setMessage(message);
        return result;
    }

    public static Result success(Object data) {
        Result result = getResult();
        result.setCode(ResultEnum.SUCCESS.getCode());
        result.setData(data);
        return result;
    }

    public static Result error() {
        Result result = getResult();
        result.setCode(ResultEnum.ERROR.getCode());
        return result;
    }

    public static Result error(Integer errorCode, String message, Object data) {
        Result result = getResult();
        result.setCode(errorCode);
        result.setMessage(message);
        result.setData(data);
        return result;
    }

    public static Result error(Integer errorCode, String message) {
        Result result = getResult();
        result.setCode(errorCode);
        result.setMessage(message);
        return result;
    }

    public static Result error(Object data) {
        Result result = getResult();
        result.setCode(ResultEnum.ERROR.getCode());
        result.setData(data);
        return result;
    }
}
