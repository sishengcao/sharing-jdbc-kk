package com.kk.common.menu;

/**
 * @auther sishengcao
 * @date 2022/4/13 17:18
 */
public enum ResultEnum {

    SUCCESS(2000,"请求成功"),
    ERROR(5000,"请求失败")
    ;

    private Integer code;
    private String name;

    public Integer getCode(){
        return this.code;
    }

    public String getName(){
        return this.name;
    }
    ResultEnum(Integer code,String name){
        this.code = code;
        this.name = name;
    }
}
