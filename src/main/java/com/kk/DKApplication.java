package com.kk;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.kk.mapper")
public class DKApplication {

    public static void main(String[] args) {
        SpringApplication.run(DKApplication.class, args);
    }

}
