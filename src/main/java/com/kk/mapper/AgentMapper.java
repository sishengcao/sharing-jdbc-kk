package com.kk.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kk.entry.Agent;
import org.apache.ibatis.annotations.Mapper;

/**
 * @auther sishengcao
 * @date 2022/4/13 16:13
 */
@Mapper
public interface AgentMapper extends BaseMapper<Agent> {
}
