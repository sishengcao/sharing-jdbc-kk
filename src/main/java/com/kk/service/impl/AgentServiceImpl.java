package com.kk.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kk.entry.Agent;
import com.kk.mapper.AgentMapper;
import com.kk.service.IAgentService;
import org.springframework.stereotype.Service;

/**
 * @auther sishengcao
 * @date 2022/4/13 16:30
 */
@Service
public class AgentServiceImpl extends ServiceImpl<AgentMapper, Agent> implements IAgentService {
}
