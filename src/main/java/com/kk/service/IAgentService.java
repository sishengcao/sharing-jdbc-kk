package com.kk.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kk.entry.Agent;

/**
 * @auther sishengcao
 * @date 2022/4/13 16:29
 */
public interface IAgentService extends IService<Agent> {
}
