package com.kk.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @auther sishengcao
 * @date 2022/4/13 10:44
 */
public class AesUtil {
    // 编码
    private static final String ENCODING = "UTF-8";
    // 算法定义
    private final static String AES = "AES";
    // 指定填充方式
    private static final String CIPHER_CBC_PADDING = "AES/CBC/PKCS5Padding";
    // 偏移量(CBC中使用，增强加密算法强度)
    private static final String IV_SEED = "1234567812345678";
    private final static int HEX2BYTE_2 = 2;
    private final static int HEX2BYTE_16 = 16;
    private static char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    // aesKey //46EBA22EF5204DD5B110A1F730513965
    private static final String aesKey = "46DFA22EF5204DD5B110A1F995050188";

    public static String encryptByAES(String content){
        try {
            SecretKeySpec key = getKeySpecFromBytes(aesKey.toUpperCase());
            Cipher cipher = Cipher.getInstance(CIPHER_CBC_PADDING);
            IvParameterSpec iv = new IvParameterSpec(IV_SEED.getBytes(ENCODING));
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            byte[] result = cipher.doFinal(content.getBytes("UTF-8"));
            return toHexString(result);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static String decryptByAES(String content){
        try {
            SecretKeySpec key = getKeySpecFromBytes(aesKey.toUpperCase());
            // 偏移
            IvParameterSpec iv = new IvParameterSpec(IV_SEED.getBytes(ENCODING));
            Cipher cipher = Cipher.getInstance(CIPHER_CBC_PADDING);
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            byte[] result = cipher.doFinal(hex2byte(content.getBytes("UTF-8")));
            return new String(result);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    private static SecretKeySpec getKeySpecFromBytes(String strBytes) {
        SecretKeySpec spec = new SecretKeySpec(hex2byte(strBytes.getBytes()), AES);
        return spec;
    }
    private static byte[] hex2byte(byte[] b) {
        if (b.length % HEX2BYTE_2 != 0) {
            throw new IllegalArgumentException("长度不是偶数!");
        }
        byte[] b2 = new byte[b.length / HEX2BYTE_2];
        for (int n = 0; n < b.length; n += HEX2BYTE_2) {
            String item = new String(b, n, HEX2BYTE_2);
            b2[(n / HEX2BYTE_2)] = (byte) Integer.parseInt(item, HEX2BYTE_16);
        }
        return b2;
    }
    private static String toHexString(byte[] byteArray) {
        StringBuffer sb = new StringBuffer(byteArray.length * 2);
        for (int i = 0; i < byteArray.length; i++) {
            sb.append(hexChar[((byteArray[i] & 0xF0) >>> 4)]);
            sb.append(hexChar[(byteArray[i] & 0xF)]);
        }
        return sb.toString();
    }

    public static void main(String[] args) throws Exception {

        String content="测试";
        String s = encryptByAES(content);
        System.out.println(s);

        String s1 = decryptByAES(s);
        System.out.println(s1);

    }
}
