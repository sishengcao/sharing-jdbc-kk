package com.kk.entry;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @auther sishengcao
 * @date 2022/4/13 16:00
 */
@Data
@TableName("tbl_agent")
public class Agent implements Serializable {
    private static final long serialVersionUID = -1L;

    /** 主键，代理商序号 */
    @TableId(type = IdType.AUTO)
    private Long id;

    private Integer orderId;

    /** 代理商全称 */
    private String name;

    /** 代理商简码 */
    private String code;

    /** 状态（0 删除 1 存在） */
    private Integer status;

    /** 创建时间 */
    private Date createTime;

    /** 更新时间 */
    private Date updateTime;
}
