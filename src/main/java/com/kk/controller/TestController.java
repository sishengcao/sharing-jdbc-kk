package com.kk.controller;

import com.kk.common.response.Result;
import com.kk.entry.Agent;
import com.kk.service.IAgentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @auther sishengcao
 * @date 2022/4/13 16:33
 */
@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private IAgentService agentService;

    @GetMapping("/agentSave")
    public Result batchInsert(){
        for (int i=1;i<=200;i++){
            Agent agent = new Agent();
            agent.setOrderId(i);
            agent.setCode("CODE_" + i);
            agent.setName("name_" + 1);
            agent.setCreateTime(new Date());
            agent.setUpdateTime(new Date());
            agent.setStatus(1);
            agent.setOrderId(i);
            agentService.save(agent);
        }
        return Result.success();
    }

    @GetMapping("/getById/{id}")
    public Result getById(@PathVariable("id")Integer id){
        return Result.success(agentService.getById(id));
    }
}
