package com.kk.config;

import io.shardingsphere.api.algorithm.sharding.PreciseShardingValue;
import io.shardingsphere.api.algorithm.sharding.RangeShardingValue;
import io.shardingsphere.api.algorithm.sharding.ShardingValue;
import io.shardingsphere.api.algorithm.sharding.complex.ComplexKeysShardingAlgorithm;
import io.shardingsphere.api.algorithm.sharding.hint.HintShardingAlgorithm;
import io.shardingsphere.api.algorithm.sharding.standard.PreciseShardingAlgorithm;
import io.shardingsphere.api.algorithm.sharding.standard.RangeShardingAlgorithm;
import io.shardingsphere.api.config.strategy.*;

import java.util.Collection;

/**
 * 分片规则
 * @auther sishengcao
 * @date 2022/5/10 9:23
 */
public class StrategyConfig {

    /**
     *  Collection collection : 集合 , 如果是表配置,则是tbl_agent0,tbl_agent1 . 如果是库配置 ,则是配置的ds0,ds1
     *  loginTableName : 即设置的loginTable --> tbl_agent
     *  columName : 配置的列 --> order_id
     *  value : 列设置的值
     */

    /**
     * ShardingStrategyConfiguration的实现类，用于单分片键的标准分片场景
     *
     * shardingColumn	String	分片列名称
     * preciseShardingAlgorithm	PreciseShardingAlgorithm	精确分片算法，用于=和IN
     * rangeShardingAlgorithm (?)	RangeShardingAlgorithm	范围分片算法，用于BETWEEN
     * @return
     */
    public static StandardShardingStrategyConfiguration standardShardingStrategyConfiguration(){
        StandardShardingStrategyConfiguration strategyConfiguration = new StandardShardingStrategyConfiguration("order_id",new MyPreciseShardingAlgorithm(),new MyRangeShardingAlgorithm());
        return strategyConfiguration;
    }

    /**
     * ShardingStrategyConfiguration的实现类，用于多分片键的复合分片场景
     *
     * shardingColumns	String	分片列名称，多个列以逗号分隔
     * shardingAlgorithm	ComplexKeysShardingAlgorithm	复合分片算法
     * @return
     */
    public static ComplexShardingStrategyConfiguration complexShardingStrategyConfiguration(){
        ComplexShardingStrategyConfiguration configuration = new ComplexShardingStrategyConfiguration("order_id",new MyComplexKeysShardingAlgorithm());
        return configuration;
    }

    /**
     * ShardingStrategyConfiguration的实现类，用于配置行表达式分片策略
     *
     * shardingColumn	String	分片列名称
     * algorithmExpression	String	分片算法行表达式，需符合groovy语法，详情请参考行表达式
     * @return
     */
    public static InlineShardingStrategyConfiguration inlineShardingStrategyConfiguration(){
        InlineShardingStrategyConfiguration configuration = new InlineShardingStrategyConfiguration("order_id", "ds${order_id % 2}");
        return configuration;
    }

    /**
     * ShardingStrategyConfiguration的实现类，用于配置Hint方式分片策略
     *
     * shardingAlgorithm	HintShardingAlgorithm	Hint分片算法
     * @return
     */
    public static HintShardingStrategyConfiguration hintShardingStrategyConfiguration(){
        HintShardingStrategyConfiguration configuration = new HintShardingStrategyConfiguration(new MyHintShardingAlgorithm());
        return configuration;
    }

    /**
     * ShardingStrategyConfiguration的实现类，用于配置不分片的策略
     * @return
     */
    public static NoneShardingStrategyConfiguration noneShardingStrategyConfiguration(){
        NoneShardingStrategyConfiguration configuration = new NoneShardingStrategyConfiguration();
        return configuration;
    }

    /**
     * 精确分片算法，用于=和IN
     */
    private static class MyPreciseShardingAlgorithm implements PreciseShardingAlgorithm {

        @Override
        public String doSharding(Collection collection, PreciseShardingValue preciseShardingValue) {
            String value = Integer.valueOf(preciseShardingValue.getValue().toString()) % 2 + "";
            for (Object item : collection){
                if(item.toString().endsWith(value)){
                    return item.toString();
                }
            }
            return null;
        }
    }

    /**
     * 范围分片算法，用于BETWEEN
     */
    private static class MyRangeShardingAlgorithm implements RangeShardingAlgorithm{

        @Override
        public Collection<String> doSharding(Collection collection, RangeShardingValue rangeShardingValue) {
            return null;
        }
    }

    /**
     * 分片列名称，多个列以逗号分隔
     */
    private static class MyComplexKeysShardingAlgorithm implements ComplexKeysShardingAlgorithm{

        @Override
        public Collection<String> doSharding(Collection<String> collection, Collection<ShardingValue> collection1) {
            return null;
        }
    }

    /**
     * Hint分片算法
     */
    private static class MyHintShardingAlgorithm implements HintShardingAlgorithm{

        @Override
        public Collection<String> doSharding(Collection<String> collection, ShardingValue shardingValue) {
            return null;
        }
    }
}
