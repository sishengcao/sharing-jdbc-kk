//package com.kk.config;
//
//import com.google.common.collect.Lists;
//import io.shardingsphere.api.algorithm.masterslave.RandomMasterSlaveLoadBalanceAlgorithm;
//import io.shardingsphere.api.config.rule.MasterSlaveRuleConfiguration;
//import io.shardingsphere.api.config.rule.ShardingRuleConfiguration;
//import io.shardingsphere.api.config.rule.TableRuleConfiguration;
//import io.shardingsphere.api.config.strategy.InlineShardingStrategyConfiguration;
//import io.shardingsphere.shardingjdbc.api.ShardingDataSourceFactory;
//import org.apache.commons.dbcp2.BasicDataSource;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.sql.DataSource;
//import java.util.*;
//
///**
// * 数据分片 + 读写分离
// * @auther sishengcao
// * @date 2022/5/9 10:16
// */
//@Configuration
//public class TogetherDatasource {
//
//    @Bean
//    public DataSource getDataSource() throws Exception {
//        ShardingRuleConfiguration shardingRuleConfig = new ShardingRuleConfiguration();
//        shardingRuleConfig.getTableRuleConfigs().add(getAgentTableRuleConfiguration());
//        shardingRuleConfig.getBindingTableGroups().add("tbl_agent");
//        shardingRuleConfig.setDefaultDatabaseShardingStrategyConfig(new InlineShardingStrategyConfiguration("order_id", "ds${order_id % 2}"));
//        shardingRuleConfig.setDefaultTableShardingStrategyConfig(new InlineShardingStrategyConfiguration("order_id", "tbl_agent${order_id % 2}"));
//        shardingRuleConfig.setMasterSlaveRuleConfigs(getMasterSlaveRuleConfigurations());
//        return ShardingDataSourceFactory.createDataSource(createDataSourceMap(), shardingRuleConfig, new HashMap<>(), new Properties());
//    }
//
//    /**
//     * 设置表 tbl_agent 的表规则
//     * @return
//     */
//    private TableRuleConfiguration getAgentTableRuleConfiguration() {
//        TableRuleConfiguration agentTableRuleConfig = new TableRuleConfiguration();
//        agentTableRuleConfig.setLogicTable("tbl_agent");
//        agentTableRuleConfig.setActualDataNodes("ds_0.tbl_agent${0..1}");
//        agentTableRuleConfig.setKeyGeneratorColumnName("order_id");
//        return agentTableRuleConfig;
//    }
//
//    private List<MasterSlaveRuleConfiguration> getMasterSlaveRuleConfigurations() {
//        MasterSlaveRuleConfiguration masterSlaveRuleConfig1 = new MasterSlaveRuleConfiguration("ds_0", "ds0", Arrays.asList("ds1", "ds2"),new RandomMasterSlaveLoadBalanceAlgorithm());
//        return Lists.newArrayList(masterSlaveRuleConfig1);
//    }
//
//    private Map<String, DataSource> createDataSourceMap() {
//        final Map<String, DataSource> result = new HashMap<>();
//        result.put("ds0", mainDataSource());
//        result.put("ds1", salver1DataSource());
//        result.put("ds2", slaver2DataSource());
//        return result;
//    }
//
//    public BasicDataSource mainDataSource(){
//        BasicDataSource masterDataSource = new BasicDataSource();
//        masterDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
//        masterDataSource.setUrl("jdbc:mysql://localhost:3306/main?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&useSSL=true&rewriteBatchedStatements=true&autoReconnect=true&failOverReadOnly=false");
//        masterDataSource.setUsername("root");
//        masterDataSource.setPassword("root");
//        return masterDataSource;
//    }
//
//    public BasicDataSource salver1DataSource(){
//        BasicDataSource masterDataSource = new BasicDataSource();
//        masterDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
//        masterDataSource.setUrl("jdbc:mysql://localhost:3306/slaver1?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&useSSL=true&rewriteBatchedStatements=true&autoReconnect=true&failOverReadOnly=false");
//        masterDataSource.setUsername("root");
//        masterDataSource.setPassword("root");
//        return masterDataSource;
//    }
//
//    public BasicDataSource slaver2DataSource(){
//        BasicDataSource masterDataSource = new BasicDataSource();
//        masterDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
//        masterDataSource.setUrl("jdbc:mysql://localhost:3306/slaver2?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&useSSL=true&rewriteBatchedStatements=true&autoReconnect=true&failOverReadOnly=false");
//        masterDataSource.setUsername("root");
//        masterDataSource.setPassword("root");
//        return masterDataSource;
//    }
//}
