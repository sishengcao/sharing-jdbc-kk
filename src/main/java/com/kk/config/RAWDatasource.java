//package com.kk.config;
//
//import io.shardingsphere.api.algorithm.masterslave.RandomMasterSlaveLoadBalanceAlgorithm;
//import io.shardingsphere.api.config.rule.MasterSlaveRuleConfiguration;
//import io.shardingsphere.shardingjdbc.api.MasterSlaveDataSourceFactory;
//import org.apache.commons.dbcp2.BasicDataSource;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.sql.DataSource;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Properties;
//
///**
// * read and write 读写分离配置
// * @auther sishengcao
// * @date 2022/5/9 10:00
// */
//@Configuration
//public class RAWDatasource {
//
//    @Bean
//    public DataSource dataSource() throws Exception{
//        // 配置真实数据源
//        Map<String, DataSource> dataSourceMap = new HashMap<>();
//
//        // 配置主库
//        BasicDataSource masterDataSource = new BasicDataSource();
//        masterDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
//        masterDataSource.setUrl("jdbc:mysql://localhost:3306/main?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&useSSL=true&rewriteBatchedStatements=true&autoReconnect=true&failOverReadOnly=false");
//        masterDataSource.setUsername("root");
//        masterDataSource.setPassword("root");
//        dataSourceMap.put("ds_master", masterDataSource);
//
//        // 配置第一个从库
//        BasicDataSource slaveDataSource1 = new BasicDataSource();
//        slaveDataSource1.setDriverClassName("com.mysql.cj.jdbc.Driver");
//        slaveDataSource1.setUrl("jdbc:mysql://localhost:3306/slaver1?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&useSSL=true&rewriteBatchedStatements=true&autoReconnect=true&failOverReadOnly=false");
//        slaveDataSource1.setUsername("root");
//        slaveDataSource1.setPassword("root");
//        dataSourceMap.put("ds_slave0", slaveDataSource1);
//
//        // 配置第二个从库
//        BasicDataSource slaveDataSource2 = new BasicDataSource();
//        slaveDataSource2.setDriverClassName("com.mysql.cj.jdbc.Driver");
//        slaveDataSource2.setUrl("jdbc:mysql://localhost:3306/slaver2?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&useSSL=true&rewriteBatchedStatements=true&autoReconnect=true&failOverReadOnly=false");
//        slaveDataSource2.setUsername("root");
//        slaveDataSource2.setPassword("root");
//        dataSourceMap.put("ds_slave1", slaveDataSource2);
//
//        // 配置读写分离规则
//        MasterSlaveRuleConfiguration masterSlaveRuleConfig = new MasterSlaveRuleConfiguration("ds_master_slave", "ds_master", Arrays.asList("ds_slave0", "ds_slave1"), new RandomMasterSlaveLoadBalanceAlgorithm());
//
//        // 获取数据源对象
//        DataSource dataSource = MasterSlaveDataSourceFactory.createDataSource(dataSourceMap, masterSlaveRuleConfig, new HashMap<>(), new Properties());
//        return dataSource;
//    }
//}
